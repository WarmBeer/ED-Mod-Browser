const seeder = "192.168.0.161:3000";

const MAX_ITEMS = 12;

let page = 1;
let pages = 1;
let mod_list = [];
let filtered_mod_list = [];
let mod_hashes = [];
let refreshing = false;

dew.command('bind F12 game.showscreen mod_browser');
hide_form();
get_local_mods();
get_mods_from_seeder();

$(document).keyup(function(e) {
    if (e.key === "Escape") { // escape key maps to keycode `27`
        close_screen();
    }
});

function get_local_mods() {
    dew.command('Server.ListMods').then(data => {
        data = JSON.parse(decodeURIComponent(data));
        for (let mod in data) {
            mod_hashes.push(data[mod].hash);
        }
    });
}

function close_screen() {
    dew.hide();
    dew.command('Game.HideH3UI 0');
}

function get_mods_from_seeder() {
    fetch("http://" + seeder + "/mods", ).then(response => {
        return response.json();
    }).then(data => {
        mod_list = data.result.mod_list;
        filter_mod_list();
    }).catch(err => {
        console.log(err);
    });
    refreshing = false;
    refresh_end();
}

function send_mod() {
    if (!!$('#form-url').val() && !!$('#form-author').val() && !!$('#form-description').val()) {
        $.get("http://" + seeder + "/mods?url=" + $('#form-url').val() + "&author=" + $('#form-author').val() + "&description=" + $('#form-description').val(), {}, function (data) {
            swal({
                title: "Alert",
                text: data.result.msg
            });
        });
        hide_form();
    }
}

function hide_form() {
    $('#submitForm').hide();
}

function show_form() {
    $('#submitForm').show();
}

function next_page() {
    page+=1;
    load_mods(page);
    set_page_controls();
}

function prev_page() {
    page-=1;
    load_mods(page);
    set_page_controls();
}

function set_page_controls() {
    $('#page').html(page);

    if (page > 1) {
        $("#prev").attr("disabled", false);
    } else {
        $("#prev").attr("disabled", true);
    }

    if (pages > page) {
        $("#next").attr("disabled", false);
    } else {
        $("#next").attr("disabled", true);
    }
}

function filter_mod_list() {
    let query = $('#query').val().toLowerCase();
    page = 1;
    filtered_mod_list = [];

    if (!!query) {
        for (let mod in mod_list) {
            if ((mod_list[mod].author + mod_list[mod].description + mod_list[mod].key).toLowerCase().indexOf(query) > -1) {
                filtered_mod_list.push(mod_list[mod]);
            }
        }  
    } else {
        filtered_mod_list = mod_list;
    }

    pages = filtered_mod_list.length / MAX_ITEMS;
    set_page_controls();
    load_mods();
}

function load_mods() {
    $('#cards').html("");

    for (let mod = (page - 1) * MAX_ITEMS; mod < filtered_mod_list.length; mod++) {
        if (mod == page * MAX_ITEMS) {
            break;
        }
        add_card(filtered_mod_list[mod]);
    }
}

function add_card(mod) {
    let div = $('<div/>', {
        class: "card",
        onclick: "update_details(this.dataset.mod)",
        "data-mod": encodeURIComponent(JSON.stringify(mod))
    });

    let img = $('<img>', {
       src: "img/placeholder.png"
    });

    let author = $('<h2/>', {
        text: mod.author
    });

    let description = $('<h4/>', {
        text: mod.description
    });

    img.appendTo(div);
    author.appendTo(div);
    description.appendTo(div);
    div.appendTo('#cards');
}

function update_details(mod) {
    $('#download-link').attr("data-mod", mod);
    mod = JSON.parse(decodeURIComponent(mod));
    $('#key').val(mod.key);
    $('#author').html(mod.author);
    $('#description').html(mod.description);
    $('#date').html(new Date(mod.date));
}

function refresh() {
    if (!refreshing) {
        let refreshButton = document.getElementById('refresh');
        refreshButton.classList.add('refreshing');
        refreshing = true;
        get_mods_from_seeder();
    }
}

function refresh_end() {
    let refreshButton = document.getElementById('refresh');
    refreshButton.classList.remove('refreshing');
    refreshing = false;
}

function install_mod(mod) {
    mod = JSON.parse(decodeURIComponent(mod));
    console.log(mod_hashes);
    console.log(mod.hash);
    if (mod_hashes.indexOf(mod.hash) < 0) {
        dew.command('Server.DownloadMod "' + mod.url.toString() + '"');
    } else {
        swal({
            text: "Mod already installed!"
        });
    }
}

dew.on("show", function() {
    dew.command('Game.HideH3UI 1');

});
